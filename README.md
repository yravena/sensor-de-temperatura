# Sensor de temperatura

O DS18B20 é um sensor de temperatura da Dallas/Maxim com saída digital programável de 9 a 12 bits. A temperatura de operação é de -55 a +125°C, com uma precisão de ±0.5°C entre -10 e +85°C.